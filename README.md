# FooBar With Dad

## Challenge: Decryption

The Caesar cypher is a simple form of encryption created by shifting the alphabet by some fixed number of positions. For a shift of 2 to the right, for example, A would map to C, B would map to D, and so on. Your challenge is to write a function that takes 3 arguments: an encoded message as a string, a direction as a string (either “left” or “right”), and a shift amount as an integer between 1 and 25. Your function should return the decrypted message. Spaces in the message should be left in place.

**Test cases**:

`solution(“abc”, right, 3)`
Should return: “def”

`solution(“gjs jrnqd hqfnwj”, “left”, 5)`
Should return: “ben emily claire”

Once you are confident in your solution use it to decrypt the following coded message:
“zshhq tajlzvsq hshs” “right” 8


Hint: You can get the ascii (numeric) value of a character in python by using the function `ord(character)`.

[This link](https://theasciicode.com.ar/) contains a table of characters and their ascii values.

Clone this repo and get started!
